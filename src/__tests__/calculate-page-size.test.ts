import { calculatePageSize } from '../calculate-page-size'

describe('Calculate page size', () => {
    it('should precalculate the amount of items in the current page', () => {
        const calculatedPageSize = calculatePageSize(17, 0, 10)
        const expectedPageSize: typeof calculatedPageSize = 10
        expect(calculatedPageSize).toEqual(expectedPageSize)
    })

    it('should only return the last remaining items if the last page is specified as current page', () => {
        const calculatedPageSize = calculatePageSize(17, 1, 10)
        const expectedPageSize: typeof calculatedPageSize = 7
        expect(calculatedPageSize).toEqual(expectedPageSize)
    })

    it('should by default return the default page size if not on the last page', () => {
        const calculatedPageSize = calculatePageSize(17, 0, 10)
        const expectedPageSize: typeof calculatedPageSize = 10
        expect(calculatedPageSize).toEqual(expectedPageSize)

        const calculatedPageSize2 = calculatePageSize(17, 3, 10)
        const expectedPageSize2: typeof calculatedPageSize2 = 10
        expect(calculatedPageSize2).toEqual(expectedPageSize2)
    })
})
