import { findValueInMap, findKeyInMap, findKeyValuePairInMap } from '../map'

describe('map utils', () => {
    const map = new Map<string, number[]>([['a', [1, 2, 4, 6]], ['b', [3, 4, 6, 2]], ['c', [1]]])
    const mockPredicate = (value: number[]) => value.includes(1)
    const mockFailingPredicate = (value: number[]) => value === ('purple daisies' as any)
    describe('findValueInMap', () => {
        it('should return the first value in a map which verifies the predicate', () => {
            expect(findValueInMap(map, mockPredicate)).toEqual(map.get('a'))
        })

        it('should return undefined if no value verifies the predicate', () => {
            expect(findValueInMap(map, mockFailingPredicate)).toEqual(undefined)
        })
    })

    describe('findKeyInMap', () => {
        it('should return first key in a map whose corresponding value verifies the predicate', () => {
            expect(findKeyInMap(map, mockPredicate)).toEqual('a')
        })
        it('should return undefined if no value verifies the predicate', () => {
            expect(findKeyInMap(map, mockFailingPredicate)).toEqual(undefined)
        })
    })

    describe('findKeyValuePairInMap', () => {
        it('should return as an object the first key-value pair in a map whose corresponding value verifies the predicate', () => {
            expect(findKeyValuePairInMap(map, mockPredicate)).toEqual({
                key: 'a',
                value: [1, 2, 4, 6],
            })
        })
        it('should return [undefined, undefined] if no value verifies the predicate', () => {
            expect(findKeyValuePairInMap(map, mockFailingPredicate)).toEqual({
                key: undefined,
                value: undefined,
            })
        })
    })
})
