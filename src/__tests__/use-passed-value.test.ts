import { usePassedValue } from '../use-passed-value'

describe('Use Passed Value function', () => {
    it('should return a CSS Object if the input is a valid CSS property', () => {
        const validProps: ExampleProps = { marginTop: '5px' }
        interface ExampleProps {
            marginTop?: string
        }
        const result = usePassedValue<ExampleProps>('marginTop').marginTop(validProps)
        expect(result).toEqual({ marginTop: '5px' })
    })
    it('should return a CSS object with the value transformed if passed a transformer argument', () => {
        const validProps: ExampleProps = { marginTop: 5 }
        interface ExampleProps {
            marginTop?: number
        }
        const result = usePassedValue<ExampleProps>(
            'marginTop',
            (value: number) => String(value) + 'px',
        ).marginTop(validProps)
        expect(result).toEqual({ marginTop: '5px' })
    })
})
