export function arrayOf(amount: number): number[] {
    const nums = [];
    for (let i = 0; i < amount; i++) {
        nums.push(i);
    }
    return nums;
}

export function isIn<T>(nums: T[]): (check: T) => boolean {
    return (check: T) => nums.includes(check);
}
