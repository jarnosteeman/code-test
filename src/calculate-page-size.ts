export function calculatePageSize(
    totalItems: number,
    currentPage: number,
    itemsPerPage: number
): number {
    const pageSize = Math.min(
        itemsPerPage,
        totalItems - currentPage * itemsPerPage
    );
    if (pageSize < 0) {
        return itemsPerPage;
    }
    return pageSize;
}
