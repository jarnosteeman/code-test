export function ifThen(
    bool: Boolean,
    func: Function,
    elseFunc?: Function
): void {
    if (bool) {
        func();
    } else {
        if (elseFunc) {
            elseFunc();
        }
    }
}

export function megaBoolean(val: any): boolean {
    if (!val) {
        return false;
    }
    if (val instanceof Map || val instanceof Set) {
        return !!val.size;
    }
    if (Array.isArray(val)) {
        return !!val.length;
    }
    if (val instanceof Object) {
        return !!Object.keys(val).length;
    }
    return !!val;
}
