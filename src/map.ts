type Func = (val: number[]) => boolean;

export function findValueInMap(map: Map<string, number[]>, func: Func) {
    const { value } = findKeyValuePairInMap(map, func);
    return value;
}

export function findKeyInMap(map: Map<string, number[]>, func: Func) {
    const { key } = findKeyValuePairInMap(map, func);
    return key;
}

export function findKeyValuePairInMap(map: Map<string, number[]>, func: Func) {
    for (let [key, value] of map) {
        if (func(value)) {
            return { key, value };
        }
    }
    return { key: undefined, value: undefined };
}
