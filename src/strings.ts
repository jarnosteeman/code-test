export function capitalizeEachWord(str: string): string {
    return str
        .split(/ /)
        .map(capitalizeFirstLetter)
        .join(" ");
}

export function capitalizeFirstLetter(str: string): string {
    return str.slice(0, 1).toUpperCase() + str.slice(1);
}

export function spacesToUnderscores(str: string): string {
    return str.replace(/ /g, "_");
}

export function toSnakeCase(str: string): string {
    return str
        .replace(/(.[^A-Z]){1}([A-Z])/g, (match, s1, s2) => s1 + "_" + s2)
        .toLowerCase();
}
