type UsePassedValueApi<T> = {
    [K in keyof T]-?: (
        props: T
    ) => {
        [K in keyof T]-?: any;
    };
};

export function usePassedValue<T>(
    key: keyof T,
    transform?: (v: any) => any
): UsePassedValueApi<T> {
    return {
        [key]: (props: T) => {
            return { [key]: transform ? transform(props[key]) : props[key] };
        }
    } as UsePassedValueApi<T>;
}
